# Advent of Code

These are my solutions for the [Advent of Code](https://adventofcode.com/) coding challenge that runs annually from Dec 1 through Dec 25. Generally, Python is my go-to language so solutions are likely to be there first. I'm also using AoC to learn Rust so, once I solve in Python I'm going to attempt to recreate the solution in Rust as well if I have time.

Since the puzzles unlock when I'm asleep (and because I'm not a super speedy coder), I don't ever try for the leaderboard. Instead, this is a means to brush up on some old concepts I had forgotten and to learn new tips and tricks from others.

I'll write up interesting notes and tidbits on each day's puzzle. Those will likely only end up in the Python repo since my focus for Rust is simply figuring out what I'm doing.