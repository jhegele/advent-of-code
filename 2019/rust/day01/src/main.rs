use std::fs;

fn main() {
    println!("Part 1: {}", part1());
    println!("Part 2: {}", part2());
}

fn fuel_req(mass: i64) -> i64 {
    mass / 3 - 2
}

fn fuel_req_recursive(mass: i64) -> i64 {
    let fuel: i64 = fuel_req(mass);
    if fuel <= 0 {
        return 0;
    } else {
        return fuel + fuel_req_recursive(fuel)
    }
}

fn part1() -> i64 {
    fs::read_to_string("./input.txt")
        .unwrap()
        .lines()
        .map(|mass: &str| mass.parse::<i64>().unwrap())
        .map(|mass: i64| fuel_req(mass))
        .sum()
}

fn part2() -> i64 {
    fs::read_to_string("./input.txt")
        .unwrap()
        .lines()
        .map(|mass| mass.parse::<i64>().unwrap())
        .map(|mass| fuel_req_recursive(mass))
        .sum()
}