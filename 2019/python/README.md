# AoC 2019

Each day's solutions are in a distinct `.py` file so the solutions for
Day 1 will be in `day01.py`, e.g. Any input files necessary to solve
the problem will be in the `/inputs` directory and use a similar
naming convention so inputs for Day 1 will be at `/inputs/day01.txt`.

The `utils` library is where I'll add any useful functions or classes
that I think will be reusable during the course of AoC.

Anything interesting or noteworthy for each day is below.

## [Day 1](https://adventofcode.com/2019/day/1)

**Part 1**

Nothing too interesting here, the calculation for fuel is pretty simple.

**Part 2**

I first solved this using a while loop then went back and rewrote it to use
recursion instead. Given the size of the problem, there's no meaningful
performance gain with recursion, but I always struggle to properly
structure recursive functions so it's something I like to practice when I
get the chance.

## [Day 2](https://adventofcode.com/2019/day/2)

**Part 1**

Our first VM on day 2! This type of problem, where you need to effectively
build a virtual machine to interpret some sort of elf-specific language is
pretty common in AoC. I solved both parts using a rough outline of the VM
then went back and added a nicer implementation of the VM to the `utils`
library.

**Part 2**

I'll be honest, I _could not_ figure out what in the hell this problem was
even asking for. Still not sure if I was just misreading it but I found 
the description to be incredibly confusing and ultimately had to go to
the solutions thread on Reddit and read through some other solutions to
comprehend what was being asked for. Once I understood, the solution was
pretty simple and straightforward to brute force.

## [Day 3](https://adventofcode.com/2019/day/3)

**Part 1**

And I hit my first performance issue on Day 3. I was using lists and list
comprehension to find the intersections which worked great for the test
inputs. But the real inputs were so large that I ended up having to kill
the process as it was trying to find intersections. Made a quick update 
to use sets when building the list of intersections and that solved the
problem.

**Part 2**

Mercifully, since we only have to consider the first instance of an
intersection, solving this was fairly trivial. Since we are building the
full path for each wire, we can just find the first instance of a point
representing an intersection in each path, take the index of the element
in each path, then add them together (and add 2 since indexes are
zero-based). Then just find the smallest number among those results and
we have our solution.

## [Day 4](https://adventofcode.com/2019/day/4)

This was a deceptively clever little puzzle. My actual solution kind of
brute forced the solution using string manipulation. It worked but,
after looking at some other solutions and spending some time thinking
about the problem, I rewrote the solution to something a little more
elegant.

This instruction is really the key to making this problem vastly more
workable:

> Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).

What is not immediately intuitive here is that, because of this rule,
you can immediately discard any candidate password where the sorted
list of digits is not equivalent to the unsorted list of digits. For
example, if our candidate password is 123465, sorting the digits
gives us 123456 which is _not_ equivalent to 123465 so we can just
discard this option immediately. This leaves us with a much more
workable list of candidate passwords to work with.

**Part 1**

Since we know that any valid password will have its digits sorted,
all we have to do is count the occurrence of digits in the password
and, if at least one digit occurs two or more times, we know that
it's a valid option.

**Part 2**

Part 2 initially seems really challenging but, again, since we know
that the digits must be sorted, this becomes very simple. Like with
Part 1, we count the occurrence of digits in the password and as
long as there is at least one digit with _exactly_ two occurrences,
the password is valid.