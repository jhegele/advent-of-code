from utils import read_input

def fuel_req(mass):
    # The // operator performs "floor division" so it will divide
    # then round down to the nearest integer
    return mass // 3 - 2

# For Part 2, we need to recursively calculate the fuel required
def fuel_req_recursive(mass):
    fuel = fuel_req(mass)
    if fuel <= 0:
        return 0
    else:
        return fuel + fuel_req_recursive(fuel)

def run_tests():
    inputs = [12, 14, 1969, 100756]
    p1 = [fuel_req(mass) for mass in inputs]
    p2 = [fuel_req_recursive(mass) for mass in inputs]
    print('Part 1 Test: {}'.format(p1))
    print('Part 2 Test: {}'.format(p2))

# Read our inputs and make them integers
inputs = read_input('./inputs/day01.txt', int)
p1, p2 = 0, 0
for mass in inputs:
    p1 += fuel_req(mass)
    p2 += fuel_req_recursive(mass)

# run_tests()
print('Part 1: ', p1)
print('Part 2: ', p2)
