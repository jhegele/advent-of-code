from typing import List

def read_input(path_to_input, map_fn = None):
    with open(path_to_input, 'r') as f_input:
        lines = f_input.read().split('\n')
    if map_fn is not None:
        return map(map_fn, lines)
    return lines

# Build an IntCode VM so that we can reuse this for later questions
class IntCodeVM():

    instructions = {
        99: 'halt',
        1: 'add',
        2: 'multiply',
    }

    def __init__(self, inputs: List[int], noun: int=None, verb: int=None):
        self.inputs: List[int] = inputs
        self.pointer: int = 0
        self.noun: int = noun
        self.verb: int = verb
        if noun is not None:
            self.inputs[1] = noun
        if verb is not None:
            self.inputs[2] = verb
        self.processing: bool = False

    def halt(self):
        self.processing = False 

    def add(self, address1: int, address2: int, location: int):
        self.inputs[location] = self.inputs[address1] + self.inputs[address2]
        self.pointer += 4

    def multiply(self, address1: int, address2: int, location: int):
        self.inputs[location] = self.inputs[address1] * self.inputs[address2]
        self.pointer += 4

    def execute(self):
        self.processing = True
        while self.processing:
            instruction: str = self.instructions[self.inputs[self.pointer]]
            if instruction == 'halt':
                self.halt()
            else:
                if instruction == 'add':
                    a1: int = self.inputs[self.pointer + 1]
                    a2: int = self.inputs[self.pointer + 2]
                    loc: int = self.inputs[self.pointer + 3]
                    self.add(a1, a2, loc)
                if instruction == 'multiply':
                    a1: int = self.inputs[self.pointer + 1]
                    a2: int = self.inputs[self.pointer + 2]
                    loc: int = self.inputs[self.pointer + 3]
                    self.multiply(a1, a2, loc)
    
    def get_position(self, position: int): 
        return self.inputs[position]

    def get_inputs(self):
        return self.inputs
