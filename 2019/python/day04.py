from collections import Counter

# Just two puzzle inputs today, so no need for a separate file
rng_low = 156218
rng_high = 652527

# Build a list of candidate passwords by discarding any value where the
# sorted list of digits is not equivalent to the original list of digits.
rng = [str(pwd) for pwd in range(rng_low, rng_high + 1) if list(str(pwd)) == sorted(list(str(pwd)))]

part1 = 0
part2 = 0
for pwd in rng:
    # For part one, we are good with any repeating character
    part1 += bool(any(x >= 2 for x in Counter(pwd).values()))
    # For part two, we need a repitition of exactly 2
    part2 += bool(any(x == 2 for x in Counter(pwd).values()))

print('Part 1: ', part1)
print('Part 2: ', part2)