from utils import read_input

# Use the list of moves to build a step-by-step path taken by the wire
def wire_path(moves):
    x, y = 0, 0
    path = []
    for move in moves:
        direction = move[0]
        distance = int(move[1:])
        if direction == 'U':
            new_moves = [(new_x, y) for new_x in range(x + 1, x + distance + 1)]
            x += distance
        if direction == 'D':
            new_moves = [(new_x, y) for new_x in range(x - 1, x - distance - 1, -1)]
            x -= distance
        if direction == 'R':
            new_moves = [(x, new_y) for new_y in range(y + 1, y + distance + 1)]
            y += distance
        if direction == 'L':
            new_moves = [(x, new_y) for new_y in range(y - 1, y - distance - 1, -1)]
            y -= distance
        path += new_moves
    return path

# Find all points where the two paths intersect
def intersections(path1, path2):
    # Need to use sets here to avoid long compute times
    return set(path1).intersection(set(path2))

def mahattan_distance(points):
    return [abs(point[0]) + abs(point[1]) for point in points]

# Get the total steps taken by each path to arrive at intersections
def steps_to_intersections(path1, path2, intersections):
    return [path1.index(point) + path2.index(point) + 2 for point in intersections]

def run_tests():
    tests = [
        [
            'R8,U5,L5,D3'.split(','),
            'U7,R6,D4,L4'.split(',')
        ],
        [
            'R75,D30,R83,U83,L12,D49,R71,U7,L72'.split(','),
            'U62,R66,U55,R34,D71,R55,D58,R83'.split(',')
        ],
        [
            'R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51'.split(','),
            'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7'.split(',')
        ]
    ]
    for test in tests:
        path1 = wire_path(test[0])
        path2 = wire_path(test[1])
        intersect = intersections(path1, path2)
        distances = mahattan_distance(intersect)
        distances.sort()
        print('The shortest distance is {}.'.format(distances[0]))
        steps = steps_to_intersections(path1, path2, intersect)
        steps.sort()
        print('The shortest number of steps is {}.'.format(steps[0]))
    
# run_tests()
inputs = read_input('./inputs/day03.txt')
inputs = [i.split(',') for i in inputs]

path1 = wire_path(inputs[0])
path2 = wire_path(inputs[1])
intersect = intersections(path1, path2)
distances = mahattan_distance(intersect)
distances.sort()
print('Part 1: ', distances[0])
steps = steps_to_intersections(path1, path2, intersect)
steps.sort()
print('Part 2: ', steps[0])
